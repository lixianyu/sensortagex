//
//  DeviceSelectorWTableViewController.h
//  SensorTagEX
//
//  Created by Li Xianyu on 14-6-13.
//  Copyright (c) 2014年 Texas Instruments. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "BLEDevice.h"
#import "SensorTagAppViewController.h"

@interface DeviceSelectorW : UITableViewController <CBCentralManagerDelegate,CBPeripheralDelegate>

@property (strong,nonatomic) CBCentralManager *centralManager;
@property (strong,nonatomic) NSMutableArray *nDevices;
@property (strong,nonatomic) NSMutableArray *sensorTags;


-(NSMutableDictionary *) makeSensorTagConfiguration;
@end
