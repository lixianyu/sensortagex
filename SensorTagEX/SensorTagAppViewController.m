//
//  SensorTagApp.m
//  SensorTagEX
//
//  Created by Li Xianyu on 14-6-13.
//  Copyright (c) 2014年 Texas Instruments. All rights reserved.
//

#import "SensorTagAppViewController.h"

@interface SensorTagAppViewController ()
@property (strong, nonatomic) NSString *titleForHeader;
@end

@implementation SensorTagAppViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(id) initWithStyle:(UITableViewStyle)style andSensorTag:(BLEDevice *)andSensorTag {
    self = [super initWithStyle:style];
    if (self) {
        self.bleDevice = andSensorTag;
        if (!self.ambientTemp){
            self.ambientTemp = [[temperatureCellTemplate alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Ambient temperature"];
            self.ambientTemp.temperatureIcon.image = [UIImage imageNamed:@"temperature.png"];
            self.ambientTemp.temperatureLabel.text = @"Ambient Temperature";
            self.ambientTemp.temperature.text = @"-.-°C";
        }
        if (!self.irTemp) {
            self.irTemp = [[temperatureCellTemplate alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"IR temperature"];
            self.irTemp.temperatureIcon.image = [UIImage imageNamed:@"objecttemperature.png"];
            self.irTemp.temperatureLabel.text = @"Object Temperature";
            self.irTemp.temperature.text = @"-.-°C";
        }
        if (!self.acc) {
            self.acc = [[accelerometerCellTemplate alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Accelerometer"];
            self.acc.accLabel.text = @"Accelerometer";
            self.acc.accValueX.text = @"-";
            self.acc.accValueY.text = @"-";
            self.acc.accValueZ.text = @"-";
            self.acc.accCalibrateButton.hidden = YES;
        }
        if (!self.rH) {
            self.rH = [[temperatureCellTemplate alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Relative humidity"];
            self.rH.temperatureIcon.image = [UIImage imageNamed:@"relativehumidity.png"];
            self.rH.temperatureLabel.text = @"Relative humidity";
            self.rH.temperature.text = @"-%rH";
        }
        if (!self.mag) {
            self.mag = [[accelerometerCellTemplate alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Magnetometer"];
            self.mag.accLabel.text = @"Magnetometer";
            self.mag.accIcon.image = [UIImage imageNamed:@"magnetometer.png"];
            self.mag.accValueX.text = @"-";
            self.mag.accValueY.text = @"-";
            self.mag.accValueZ.text = @"-";
            [self.mag.accCalibrateButton addTarget:self action:@selector(handleCalibrateMag) forControlEvents:UIControlEventTouchUpInside];
            self.magSensor = [[sensorMAG3110 alloc] init];
        }
        if (!self.baro) {
            self.baro = [[temperatureCellTemplate alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Barometer"];
            self.baro.temperatureLabel.text = @"Barometer";
            self.baro.temperatureIcon.image = [UIImage imageNamed:@"barometer.png"];
            self.baro.temperature.text = @"1000mBar";
        }
        if (!self.gyro) {
            self.gyro = [[accelerometerCellTemplate alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Gyroscope"];
            self.gyro.accLabel.text = @"Gyroscope";
            self.gyro.accIcon.image = [UIImage imageNamed:@"gyroscope.png"];
            self.gyro.accValueX.text = @"-";
            self.gyro.accValueY.text = @"-";
            self.gyro.accValueZ.text = @"-";
            [self.gyro.accCalibrateButton addTarget:self action:@selector(handleCalibrateGyro) forControlEvents:UIControlEventTouchUpInside];
            self.gyroSensor = [[sensorIMU3000 alloc] init];
            
        }
       // self.baroSensor = [[sensorC953A alloc] initWithCalibrationData:characteristic.value];
        
    }
    //_textTimer=[NSTimer scheduledTimerWithTimeInterval:0.3f target:self selector:@selector(alphaFader:) userInfo:nil repeats:YES];
    
    self.currentVal = [[sensorTagValues alloc]init];
    self.vals = [[NSMutableArray alloc]init];
    
//    self.logInterval = 1.0; //1000 ms
    
//    self.logTimer = [NSTimer scheduledTimerWithTimeInterval:self.logInterval target:self selector:@selector(logValues:) userInfo:nil repeats:YES];
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    _textTimer=[NSTimer scheduledTimerWithTimeInterval:0.3f target:self selector:@selector(alphaFader:) userInfo:nil repeats:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    self.sensorsEnabled = [[NSMutableArray alloc] init];
//    if (self.bleDevice.p.state == CBPeripheralStateConnected) {
//        self.bleDevice.p.delegate = self;
//        [self configureSensorTag];
//        self.title = @"TI BLE SensorTag";
//    }
//    else {
//        self.bleDevice.manager.delegate = self;
//        [self.bleDevice.manager connectPeripheral:self.bleDevice.p options:nil];
//        self.title = @"Hai";
//    }
    self.bleDevice.manager.delegate = self;
    self.bleDevice.p.delegate = self;
    self.title = @"温、湿度计";
    [self configureSensorTag];
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [self deconfigureSensorTag];
    [_textTimer invalidate];
}

-(void)viewDidDisappear:(BOOL)animated {
    self.sensorsEnabled = nil;
    //self.bleDevice.manager.delegate = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
//    UIBarButtonItem *mailer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(sendMail:)];
    _rssiBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
    _rssiBarButtonItem.tintColor = [UIColor blackColor];
    _rssiBarButtonItem.enabled = NO;
    [self.navigationItem setRightBarButtonItem:_rssiBarButtonItem animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellType = [self.sensorsEnabled objectAtIndex:indexPath.row];
    
    if ([cellType isEqualToString:@"Ambient temperature"]) return self.ambientTemp.height;
    if ([cellType isEqualToString:@"IR temperature"]) return self.irTemp.height;
    if ([cellType isEqualToString:@"Accelerometer"]) return self.acc.height;
    if ([cellType isEqualToString:@"Humidity"]) return self.rH.height;
    if ([cellType isEqualToString:@"Magnetometer"]) return self.mag.height;
    if ([cellType isEqualToString:@"Barometer"]) return self.baro.height;
    if ([cellType isEqualToString:@"Gyroscope"]) return self.gyro.height;
    
    return 50;
}

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        if (_titleForHeader) {
            return _titleForHeader;
        }
        return @"Sensors";
    }
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.sensorsEnabled.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"%s", __func__);
    NSString *cellType = [self.sensorsEnabled objectAtIndex:indexPath.row];
    //NSLog(@"cellType = %@", cellType);
    if ([cellType isEqualToString:@"Ambient temperature"]) {
        
        return self.ambientTemp;
    }
    else if ([cellType isEqualToString:@"IR temperature"]) {
        return self.irTemp;
    }
    else if ([cellType isEqualToString:@"Accelerometer"]) {
        return self.acc;
    }
    else if ([cellType isEqualToString:@"Humidity"]) {
        return self.rH;
    }
    else if ([cellType isEqualToString:@"Barometer"]) {
        return self.baro;
    }
    else if ([cellType isEqualToString:@"Gyroscope"]) {
        return self.gyro;
    }
    else if ([cellType isEqualToString:@"Magnetometer"]) {
        return self.mag;
    }
    
    // Something has gone wrong, because we should never get here, return empty cell
    return [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Unkown Cell"];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void) configureSensorTag {
    // Configure sensortag, turning on Sensors and setting update period for sensors etc ...
    
    if (([self sensorEnabled:@"Ambient temperature active"]) || ([self sensorEnabled:@"IR temperature active"])) {
        if ([self sensorEnabled:@"Ambient temperature active"]) [self.sensorsEnabled addObject:@"Ambient temperature"];
        if ([self sensorEnabled:@"IR temperature active"]) [self.sensorsEnabled addObject:@"IR temperature"];
    }
    
//    if ([self sensorEnabled:@"Accelerometer active"]) {
//        CBUUID *sUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Accelerometer service UUID"]];
//        CBUUID *cUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Accelerometer config UUID"]];
//        CBUUID *pUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Accelerometer period UUID"]];
//        NSInteger period = [[self.bleDevice.setupData valueForKey:@"Accelerometer period"] integerValue];
//        uint8_t periodData = (uint8_t)(period / 10);
//        NSLog(@"%d",periodData);
//        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:pUUID data:[NSData dataWithBytes:&periodData length:1]];
//        uint8_t data = 0x01;
//        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:&data length:1]];
//        cUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Accelerometer data UUID"]];
//        [BLEUtility setNotificationForCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID enable:YES];
//        [self.sensorsEnabled addObject:@"Accelerometer"];
//    }
    
    if ([self sensorEnabled:@"Humidity active"]) {
        [self.sensorsEnabled addObject:@"Humidity"];
    }
    
    if ([self sensorEnabled:@"Barometer active"]) {
//        CBUUID *sUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Barometer service UUID"]];
//        CBUUID *cUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Barometer config UUID"]];
//        //Issue calibration to the device
//        uint8_t data = 0x02;
//        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:&data length:1]];
//        cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Barometer data UUID"]];
//        [BLEUtility setNotificationForCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID enable:YES];
//        
//        cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Barometer calibration UUID"]];
//        [BLEUtility readCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID];
        [self.sensorsEnabled addObject:@"Barometer"];
    }
//    if ([self sensorEnabled:@"Gyroscope active"]) {
//        CBUUID *sUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Gyroscope service UUID"]];
//        CBUUID *cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Gyroscope config UUID"]];
//        uint8_t data = 0x07;
//        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:&data length:1]];
//        cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Gyroscope data UUID"]];
//        [BLEUtility setNotificationForCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID enable:YES];
//        [self.sensorsEnabled addObject:@"Gyroscope"];
//    }
//    
//    if ([self sensorEnabled:@"Magnetometer active"]) {
//        CBUUID *sUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Magnetometer service UUID"]];
//        CBUUID *cUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Magnetometer config UUID"]];
//        CBUUID *pUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Magnetometer period UUID"]];
//        NSInteger period = [[self.bleDevice.setupData valueForKey:@"Magnetometer period"] integerValue];
//        uint8_t periodData = (uint8_t)(period / 10);
//        NSLog(@"%d",periodData);
//        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:pUUID data:[NSData dataWithBytes:&periodData length:1]];
//        uint8_t data = 0x01;
//        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:&data length:1]];
//        cUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Magnetometer data UUID"]];
//        [BLEUtility setNotificationForCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID enable:YES];
//        [self.sensorsEnabled addObject:@"Magnetometer"];
//    }
    
}

-(void) deconfigureSensorTag {
//    if (([self sensorEnabled:@"Ambient temperature active"]) || ([self sensorEnabled:@"IR temperature active"])) {
//        // Enable Temperature sensor
//        CBUUID *sUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"IR temperature service UUID"]];
//        CBUUID *cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"IR temperature config UUID"]];
//        unsigned char data = 0x00;
//        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:&data length:1]];
//        cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"IR temperature data UUID"]];
//        [BLEUtility setNotificationForCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID enable:NO];
//    }
//    if ([self sensorEnabled:@"Accelerometer active"]) {
//        CBUUID *sUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Accelerometer service UUID"]];
//        CBUUID *cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Accelerometer config UUID"]];
//        uint8_t data = 0x00;
//        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:&data length:1]];
//        cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Accelerometer data UUID"]];
//        [BLEUtility setNotificationForCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID enable:NO];
//    }
//    if ([self sensorEnabled:@"Humidity active"]) {
//        CBUUID *sUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Humidity service UUID"]];
//        CBUUID *cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Humidity config UUID"]];
//        uint8_t data = 0x00;
//        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:&data length:1]];
//        cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Humidity data UUID"]];
//        [BLEUtility setNotificationForCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID enable:NO];
//    }
//    if ([self sensorEnabled:@"Magnetometer active"]) {
//        CBUUID *sUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Magnetometer service UUID"]];
//        CBUUID *cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Magnetometer config UUID"]];
//        uint8_t data = 0x00;
//        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:&data length:1]];
//        cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Magnetometer data UUID"]];
//        [BLEUtility setNotificationForCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID enable:NO];
//    }
//    if ([self sensorEnabled:@"Gyroscope active"]) {
//        CBUUID *sUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Gyroscope service UUID"]];
//        CBUUID *cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Gyroscope config UUID"]];
//        uint8_t data = 0x00;
//        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:&data length:1]];
//        cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Gyroscope data UUID"]];
//        [BLEUtility setNotificationForCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID enable:NO];
//    }
//    if ([self sensorEnabled:@"Barometer active"]) {
//        CBUUID *sUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Barometer service UUID"]];
//        CBUUID *cUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Barometer config UUID"]];
//        //Disable sensor
//        uint8_t data = 0x00;
//        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:&data length:1]];
//        cUUID =  [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Barometer data UUID"]];
//        [BLEUtility setNotificationForCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID enable:NO];
//        
//    }
}

-(bool)sensorEnabled:(NSString *)Sensor {
    NSString *val = [self.bleDevice.setupData valueForKey:Sensor];
    if (val) {
        if ([val isEqualToString:@"1"]) return TRUE;
    }
    return FALSE;
}

-(int)sensorPeriod:(NSString *)Sensor {
    NSString *val = [self.bleDevice.setupData valueForKey:Sensor];
    return [val integerValue];
}



#pragma mark - CBCentralManager delegate function

-(void) centralManagerDidUpdateState:(CBCentralManager *)central {
    
}

//-(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
//    
//    NSLog(@"%s,Found a BLE Device : %@, RSSI = %@, advertisementData = %@", __func__, peripheral, RSSI, advertisementData);
//    NSLog(@"name = %@", [peripheral name]);
//    
//    /* iOS 6.0 bug workaround : connect to device before displaying UUID !
//     The reason for this is that the CFUUID .UUID property of CBPeripheral
//     here is null the first time an unkown (never connected before in any app)
//     peripheral is connected. So therefore we connect to all peripherals we find.
//     */
//    if (![_bleDevice.p.identifier.UUIDString isEqualToString:peripheral.identifier.UUIDString]) {
//        NSLog(@"%@:%@, not equal, so just return.", _bleDevice.p.identifier.UUIDString, peripheral.identifier.UUIDString);
//        return;
//    }
//    peripheral.delegate = self;
//    
////    if ([[peripheral name] isEqualToString:@"TI BLE Sensor Tag"]) {
////        NSLog(@"This is a SensorTag !");
////        BOOL replace = NO;
////        // Match if we have this device from before
////        for (int ii=0; ii < self.sensorTags.count; ii++) {
////            CBPeripheral *p = [self.sensorTags objectAtIndex:ii];
////            if ([p isEqual:peripheral]) {
////                [self.sensorTags replaceObjectAtIndex:ii withObject:peripheral];
////                replace = YES;
////            }
////        }
////        if (!replace) {
////            [self.sensorTags addObject:peripheral];
////            [self.tableView reloadData];
////        }
////    }
//    
//    _rssiBarButtonItem.title = [NSString stringWithFormat:@"%@dBm", RSSI];
//    NSArray *keys = [advertisementData allKeys];
//    NSData *dataAmb, *dataObj, *dataHum, *dataHum1,*dataBar,*dataBar1;
//    for (int i = 0; i < [keys count]; ++i) {
//        id key = [keys objectAtIndex: i];
//        NSString *keyName = (NSString *) key;
//        NSObject *value = [advertisementData objectForKey: key];
//        if ([value isKindOfClass: [NSArray class]]) {
//            printf("   key: %s\n", [keyName cStringUsingEncoding: NSUTF8StringEncoding]);
//            NSArray *values = (NSArray *) value;
//            for (int j = 0; j < [values count]; ++j) {
//                if ([[values objectAtIndex: j] isKindOfClass: [CBUUID class]]) {
//                    CBUUID *uuid = [values objectAtIndex: j];
//                    NSData *data = uuid.data;
//                    if (j == 1) {
//                        dataObj = uuid.data;
//                    } else if (j == 2) {
//                        dataAmb = uuid.data;
//                    } else if (j == 3) {
//                        dataHum = uuid.data;
//                    } else if (j == 4) {
//                        dataHum1 = uuid.data;
//                    } else if (j == 5) {
//                        dataBar = uuid.data;
//                    } else if (j == 6) {
//                        dataBar1 = uuid.data;
//                    }
//                    printf("      uuid(%d):", j);
//                    for (int j = 0; j < data.length; ++j)
//                        printf(" %02X", ((UInt8 *) data.bytes)[j]);
//                    printf("\n");
//                } else {
//                    const char *valueString = [[value description] cStringUsingEncoding: NSUTF8StringEncoding];
//                    printf("      value(%d): %s\n", j, valueString);
//                }
//            }
//        } else {
//            const char *valueString = [[value description] cStringUsingEncoding: NSUTF8StringEncoding];
//            printf("   key: %s, value: %s\n", [keyName cStringUsingEncoding: NSUTF8StringEncoding], valueString);
//        }
//    }
//    uint8_t amb[2];
//    [dataAmb getBytes:amb length:dataAmb.length];
//    uint8_t obj[2];
//    [dataObj getBytes:obj length:dataObj.length];
//    uint8_t ambobj[4];
//    ambobj[0] = obj[1];
//    ambobj[1] = obj[0];
//    ambobj[2] = amb[1];
//    ambobj[3] = amb[0];
//    NSLog(@"0x%x, 0x%x, 0x%x, 0x%x", ambobj[0], ambobj[1], ambobj[2], ambobj[3]);
//    NSData *realObj = [NSData dataWithBytes:ambobj length:4];
//    float tAmb = [sensorTMP006 calcTAmb:realObj] - 1;
//    float tObj = [sensorTMP006 calcTObj:realObj];
//    
//    self.ambientTemp.temperature.text = [NSString stringWithFormat:@"%.1f°C",tAmb];
//    self.ambientTemp.temperature.textColor = [UIColor blackColor];
//    self.ambientTemp.temperatureGraph.progress = (tAmb / 100.0) + 0.5;
//    self.irTemp.temperature.text = [NSString stringWithFormat:@"%.1f°C",tObj];
//    self.irTemp.temperatureGraph.progress = (tObj / 1000.0) + 0.5;
//    self.irTemp.temperature.textColor = [UIColor blackColor];
//    
//    self.currentVal.tAmb = tAmb;
//    self.currentVal.tIR = tObj;
//    
//    
//    /////////////////////Humidity///////////////
//    uint8_t hum[2];
//    [dataHum getBytes:hum length:dataHum.length];
//    uint8_t hum1[2];
//    [dataHum1 getBytes:hum1 length:dataHum1.length];
//    uint8_t hums[4];
//    hums[0] = hum[0];
//    hums[1] = hum[1];
//    hums[2] = hum1[0];
//    hums[3] = hum1[1];
//    NSData *realHum = [NSData dataWithBytes:hums length:4];
//    float rHVal = [sensorSHT21 calcPress:realHum];
//    self.rH.temperature.text = [NSString stringWithFormat:@"%0.1f%%rH",rHVal];
//    self.rH.temperatureGraph.progress = (rHVal / 100);
//    self.rH.temperature.textColor = [UIColor blackColor];
//    self.currentVal.humidity = rHVal;
//    
//    /////////////////////Barometer///////////////
////    int pressure = [self.baroSensor calcPressure:dataBar];
////    self.baro.temperature.text = [NSString stringWithFormat:@"%d mBar",pressure];
////    self.baro.temperatureGraph.progress = ((float)((float)pressure - (float)800) / (float)400);
////    self.baro.temperature.textColor = [UIColor blackColor];
////    
////    self.currentVal.press = pressure;
//    
//    /////////////////////////////////
//    [self.tableView reloadData];
//}

-(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    
    NSLog(@"%s,Found a BLE Device : %@, RSSI = %@, advertisementData = %@", __func__, peripheral, RSSI, advertisementData);
    NSLog(@"name = %@", [peripheral name]);
    
    /* iOS 6.0 bug workaround : connect to device before displaying UUID !
     The reason for this is that the CFUUID .UUID property of CBPeripheral
     here is null the first time an unkown (never connected before in any app)
     peripheral is connected. So therefore we connect to all peripherals we find.
     */
    if (![_bleDevice.p.identifier.UUIDString isEqualToString:peripheral.identifier.UUIDString]) {
        NSLog(@"%@:%@, not equal, so just return.", _bleDevice.p.identifier.UUIDString, peripheral.identifier.UUIDString);
        return;
    }
    peripheral.delegate = self;
    
    _rssiBarButtonItem.title = [NSString stringWithFormat:@"%@dBm", RSSI];
    
    NSString *localName = advertisementData[@"kCBAdvDataLocalName"];
    NSLog(@"localName = %@", localName);
    if ([localName isEqualToString:@"Phobost"]) {
        
        NSData *manData =advertisementData[@"kCBAdvDataManufacturerData"];
        uint8_t tempData[12];
        [manData getBytes:tempData length:12];
        //((scratchVal[2] & 0xff)| ((scratchVal[3] << 8) & 0xff00));
        uint16_t u16Data = tempData[8] | ((tempData[9] << 8) & 0xFF00);
        u16Data = u16Data >> 4;
        float temperature = (float)u16Data * 0.0625;
        self.ambientTemp.temperature.text = [NSString stringWithFormat:@"%.1f°C",temperature];
        self.ambientTemp.temperature.textColor = [UIColor redColor];
        self.ambientTemp.temperatureGraph.progress = (temperature / 100.0) + 0.5;
        self.irTemp.temperature.text = [NSString stringWithFormat:@"%.1f°C",temperature];
        self.irTemp.temperatureGraph.progress = (temperature / 1000.0) + 0.5;
        self.irTemp.temperature.textColor = [UIColor blackColor];
        self.rH.temperature.text = [NSString stringWithFormat:@"%0.1f%%rH",30.3];
        self.rH.temperatureGraph.progress = (30.3 / 100);
        self.rH.temperature.textColor = [UIColor blackColor];
        
        self.currentVal.tAmb = temperature;
        self.currentVal.tIR = temperature;
        self.currentVal.humidity = 30.3;
        
        //计算蓝牙的唯一序列号
        char buffer[64] = {0};
        char tempBuf[12];
        for (int j = 7; j >= 2; j--) {
            sprintf(tempBuf, "%02X", tempData[j]);
            strcat(buffer, tempBuf);
        }
        strcat(buffer, ", Battery : ");
        sprintf(tempBuf, "%d%%", tempData[11]);
        strcat(buffer, tempBuf);

        printf("hoo, buffer is %s\n", buffer);
        _titleForHeader = [NSString stringWithUTF8String:buffer];

        [self.tableView reloadData];
        return;
    }

    
    NSArray *uuids = advertisementData[@"CBAdvertisementDataServiceUUIDsKey"];
    NSLog(@"uuids = %@", uuids);
    
    NSArray *keys = [advertisementData allKeys];
    NSData *dataAmb, *dataObj, *dataHum, *dataHum1,*dataBar,*dataBar1;
    for (int i = 0; i < [keys count]; ++i) {
        id key = [keys objectAtIndex: i];
        NSString *keyName = (NSString *) key;
        
        NSObject *value = [advertisementData objectForKey: key];
        NSLog(@"keyName = %@ , value = %@", keyName, value);
        
        if ([value isKindOfClass: [NSArray class]]) {
            printf("haha   key: %s\n", [keyName cStringUsingEncoding: NSUTF8StringEncoding]);
            NSArray *values = (NSArray *) value;
            for (int j = 0; j < [values count]; ++j) {
                if ([[values objectAtIndex: j] isKindOfClass: [CBUUID class]]) {
                    CBUUID *uuid = [values objectAtIndex: j];
                    NSData *data = uuid.data;
                    if (j == 1) {
                        dataObj = uuid.data;
                    } else if (j == 2) {
                        dataAmb = uuid.data;
                    } else if (j == 3) {
                        dataHum = uuid.data;
                    } else if (j == 4) {
                        dataHum1 = uuid.data;
                    } else if (j == 5) {
                        dataBar = uuid.data;
                    } else if (j == 6) {
                        dataBar1 = uuid.data;
                    }
                    printf("      uuid(%d):", j);
                    for (int j = 0; j < data.length; ++j)
                        printf(" %02X", ((UInt8 *) data.bytes)[j]);
                    printf("\n");
                } else {
                    const char *valueString = [[value description] cStringUsingEncoding: NSUTF8StringEncoding];
                    printf("      value(%d): %s\n", j, valueString);
                }
            }
        } else {
            const char *valueString = [[value description] cStringUsingEncoding: NSUTF8StringEncoding];
            printf("   key: %s, value: %s\n", [keyName cStringUsingEncoding: NSUTF8StringEncoding], valueString);
        }
    }
    uint8_t amb[2];
    [dataAmb getBytes:amb length:dataAmb.length];
    uint8_t obj[2];
    [dataObj getBytes:obj length:dataObj.length];
    uint8_t ambobj[4];
    ambobj[0] = obj[1];
    ambobj[1] = obj[0];
    ambobj[2] = amb[1];
    ambobj[3] = amb[0];
    NSLog(@"0x%x, 0x%x, 0x%x, 0x%x", ambobj[0], ambobj[1], ambobj[2], ambobj[3]);
    NSData *realObj = [NSData dataWithBytes:ambobj length:4];
    float tAmb = [sensorTMP006 calcTAmb:realObj] ;
    float tObj = [sensorTMP006 calcTObj:realObj];
    
    self.ambientTemp.temperature.text = [NSString stringWithFormat:@"%.1f°C",tAmb];
    self.ambientTemp.temperature.textColor = [UIColor redColor];
    self.ambientTemp.temperatureGraph.progress = (tAmb / 100.0) + 0.5;
    self.irTemp.temperature.text = [NSString stringWithFormat:@"%.1f°C",tObj];
    self.irTemp.temperatureGraph.progress = (tObj / 1000.0) + 0.5;
    self.irTemp.temperature.textColor = [UIColor blackColor];
    
    self.currentVal.tAmb = tAmb;
    self.currentVal.tIR = tObj;
    
    
    /////////////////////Humidity///////////////
    uint8_t hum[2];
    [dataHum getBytes:hum length:dataHum.length];
    uint8_t hum1[2];
    [dataHum1 getBytes:hum1 length:dataHum1.length];
    uint8_t hums[4];
    hums[0] = hum[0];
    hums[1] = hum[1];
    hums[2] = hum1[0];
    hums[3] = hum1[1];
    NSData *realHum = [NSData dataWithBytes:hums length:4];
    float rHVal = [sensorSHT21 calcPress:realHum];
    self.rH.temperature.text = [NSString stringWithFormat:@"%0.1f%%rH",rHVal];
    self.rH.temperatureGraph.progress = (rHVal / 100);
    self.rH.temperature.textColor = [UIColor blackColor];
    self.currentVal.humidity = rHVal;
    
    /////////////////////Barometer///////////////
    //    int pressure = [self.baroSensor calcPressure:dataBar];
    //    self.baro.temperature.text = [NSString stringWithFormat:@"%d mBar",pressure];
    //    self.baro.temperatureGraph.progress = ((float)((float)pressure - (float)800) / (float)400);
    //    self.baro.temperature.textColor = [UIColor blackColor];
    //
    //    self.currentVal.press = pressure;
    
    /////////////////////////////////
    [self.tableView reloadData];
}

-(void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    peripheral.delegate = self;
    [peripheral discoverServices:nil];
}


#pragma mark - CBperipheral delegate functions

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"..");
    if ([service.UUID isEqual:[CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Gyroscope service UUID"]]]) {
        [self configureSensorTag];
    }
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@".");
    for (CBService *s in peripheral.services) [peripheral discoverCharacteristics:nil forService:s];
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"didUpdateNotificationStateForCharacteristic %@, error = %@",characteristic.UUID, error);
}

-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    //NSLog(@"didUpdateValueForCharacteristic = %@",characteristic.UUID);
    
    
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"IR temperature data UUID"]]]) {
        float tAmb = [sensorTMP006 calcTAmb:characteristic.value];
        float tObj = [sensorTMP006 calcTObj:characteristic.value];
        
        self.ambientTemp.temperature.text = [NSString stringWithFormat:@"%.1f°C",tAmb];
        self.ambientTemp.temperature.textColor = [UIColor blackColor];
        self.ambientTemp.temperatureGraph.progress = (tAmb / 100.0) + 0.5;
        self.irTemp.temperature.text = [NSString stringWithFormat:@"%.1f°C",tObj];
        self.irTemp.temperatureGraph.progress = (tObj / 1000.0) + 0.5;
        self.irTemp.temperature.textColor = [UIColor blackColor];
        
        self.currentVal.tAmb = tAmb;
        self.currentVal.tIR = tObj;
        
        
    }
    
    
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Accelerometer data UUID"]]]) {
        float x = [sensorKXTJ9 calcXValue:characteristic.value];
        float y = [sensorKXTJ9 calcYValue:characteristic.value];
        float z = [sensorKXTJ9 calcZValue:characteristic.value];
        
        self.acc.accValueX.text = [NSString stringWithFormat:@"X: % 0.1fG",x];
        self.acc.accValueY.text = [NSString stringWithFormat:@"Y: % 0.1fG",y];
        self.acc.accValueZ.text = [NSString stringWithFormat:@"Z: % 0.1fG",z];
        
        self.acc.accValueX.textColor = [UIColor blackColor];
        self.acc.accValueY.textColor = [UIColor blackColor];
        self.acc.accValueZ.textColor = [UIColor blackColor];
        
        self.acc.accGraphX.progress = (x / [sensorKXTJ9 getRange]) + 0.5;
        self.acc.accGraphY.progress = (y / [sensorKXTJ9 getRange]) + 0.5;
        self.acc.accGraphZ.progress = (z / [sensorKXTJ9 getRange]) + 0.5;
        
        self.currentVal.accX = x;
        self.currentVal.accY = y;
        self.currentVal.accZ = z;
        
    }
    
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Humidity data UUID"]]]) {
        
        float rHVal = [sensorSHT21 calcPress:characteristic.value];
        self.rH.temperature.text = [NSString stringWithFormat:@"%0.1f%%rH",rHVal];
        self.rH.temperatureGraph.progress = (rHVal / 100);
        self.rH.temperature.textColor = [UIColor blackColor];
        
        self.currentVal.humidity = rHVal;
        
    }
    
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Magnetometer data UUID"]]]) {
        
        float x = [self.magSensor calcXValue:characteristic.value];
        float y = [self.magSensor calcYValue:characteristic.value];
        float z = [self.magSensor calcZValue:characteristic.value];
        
        self.mag.accValueX.text = [NSString stringWithFormat:@"X: % 0.1fuT",x];
        self.mag.accValueY.text = [NSString stringWithFormat:@"Y: % 0.1fuT",y];
        self.mag.accValueZ.text = [NSString stringWithFormat:@"Z: % 0.1fuT",z];
        
        self.mag.accValueX.textColor = [UIColor blackColor];
        self.mag.accValueY.textColor = [UIColor blackColor];
        self.mag.accValueZ.textColor = [UIColor blackColor];
        
        self.mag.accGraphX.progress = (x / [sensorMAG3110 getRange]) + 0.5;
        self.mag.accGraphY.progress = (y / [sensorMAG3110 getRange]) + 0.5;
        self.mag.accGraphZ.progress = (z / [sensorMAG3110 getRange]) + 0.5;
        
        self.currentVal.magX = x;
        self.currentVal.magY = y;
        self.currentVal.magZ = z;
        
    }
    
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Barometer calibration UUID"]]]) {
        
        self.baroSensor = [[sensorC953A alloc] initWithCalibrationData:characteristic.value];
        
        CBUUID *sUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Barometer service UUID"]];
        CBUUID *cUUID = [CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Barometer config UUID"]];
        //Issue normal operation to the device
        uint8_t data = 0x01;
        [BLEUtility writeCharacteristic:self.bleDevice.p sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:&data length:1]];
        
    }
    
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Barometer data UUID"]]]) {
        int pressure = [self.baroSensor calcPressure:characteristic.value];
        self.baro.temperature.text = [NSString stringWithFormat:@"%d mBar",pressure];
        self.baro.temperatureGraph.progress = ((float)((float)pressure - (float)800) / (float)400);
        self.baro.temperature.textColor = [UIColor blackColor];
        
        self.currentVal.press = pressure;
        
    }
    
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:[self.bleDevice.setupData valueForKey:@"Gyroscope data UUID"]]]) {
        
        float x = [self.gyroSensor calcXValue:characteristic.value];
        float y = [self.gyroSensor calcYValue:characteristic.value];
        float z = [self.gyroSensor calcZValue:characteristic.value];
        
        self.gyro.accValueX.text = [NSString stringWithFormat:@"X: % 0.1f°/S",x];
        self.gyro.accValueY.text = [NSString stringWithFormat:@"Y: % 0.1f°/S",y];
        self.gyro.accValueZ.text = [NSString stringWithFormat:@"Z: % 0.1f°/S",z];
        
        self.gyro.accValueX.textColor = [UIColor blackColor];
        self.gyro.accValueY.textColor = [UIColor blackColor];
        self.gyro.accValueZ.textColor = [UIColor blackColor];
        
        self.gyro.accGraphX.progress = (x / [sensorIMU3000 getRange]) + 0.5;
        self.gyro.accGraphY.progress = (y / [sensorIMU3000 getRange]) + 0.5;
        self.gyro.accGraphZ.progress = (z / [sensorIMU3000 getRange]) + 0.5;
        
        self.currentVal.gyroX = x;
        self.currentVal.gyroY = y;
        self.currentVal.gyroZ = z;
        
    }
    [self.tableView reloadData];
}

-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"didWriteValueForCharacteristic %@ error = %@",characteristic.UUID,error);
}



- (IBAction) handleCalibrateMag {
    NSLog(@"Calibrate magnetometer pressed !");
    [self.magSensor calibrate];
}
- (IBAction) handleCalibrateGyro {
    NSLog(@"Calibrate gyroscope pressed ! ");
    [self.gyroSensor calibrate];
}

-(void) alphaFader:(NSTimer *)timer {
    NSLog(@"%s", __func__);
    CGFloat w,a;
    if (self.ambientTemp) {
        [self.ambientTemp.temperature.textColor getWhite:&w alpha:&a];
        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
        self.ambientTemp.temperature.textColor = [self.ambientTemp.temperature.textColor colorWithAlphaComponent:a];
    }
    if (self.irTemp) {
        [self.irTemp.temperature.textColor getWhite:&w alpha:&a];
        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
        self.irTemp.temperature.textColor = [self.irTemp.temperature.textColor colorWithAlphaComponent:a];
    }
//    if (self.acc) {
//        [self.acc.accValueX.textColor getWhite:&w alpha:&a];
//        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
//        self.acc.accValueX.textColor = [self.acc.accValueX.textColor colorWithAlphaComponent:a];
//        
//        [self.acc.accValueY.textColor getWhite:&w alpha:&a];
//        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
//        self.acc.accValueY.textColor = [self.acc.accValueY.textColor colorWithAlphaComponent:a];
//        
//        [self.acc.accValueZ.textColor getWhite:&w alpha:&a];
//        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
//        self.acc.accValueZ.textColor = [self.acc.accValueZ.textColor colorWithAlphaComponent:a];
//    }
    if (self.rH) {
        [self.rH.temperature.textColor getWhite:&w alpha:&a];
        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
        self.rH.temperature.textColor = [self.rH.temperature.textColor colorWithAlphaComponent:a];
    }
//    if (self.mag) {
//        NSLog(@"self.mag");
//        [self.mag.accValueX.textColor getWhite:&w alpha:&a];
//        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
//        self.mag.accValueX.textColor = [self.mag.accValueX.textColor colorWithAlphaComponent:a];
//        
//        [self.mag.accValueY.textColor getWhite:&w alpha:&a];
//        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
//        self.mag.accValueY.textColor = [self.mag.accValueY.textColor colorWithAlphaComponent:a];
//        
//        [self.mag.accValueZ.textColor getWhite:&w alpha:&a];
//        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
//        self.mag.accValueZ.textColor = [self.mag.accValueZ.textColor colorWithAlphaComponent:a];
//    }
//    if (self.baro) {
//        NSLog(@"self.baro");
//        [self.baro.temperature.textColor getWhite:&w alpha:&a];
//        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
//        self.baro.temperature.textColor = [self.baro.temperature.textColor colorWithAlphaComponent:a];
//    }
//    if (self.gyro) {
//        [self.gyro.accValueX.textColor getWhite:&w alpha:&a];
//        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
//        self.gyro.accValueX.textColor = [self.gyro.accValueX.textColor colorWithAlphaComponent:a];
//        
//        [self.gyro.accValueY.textColor getWhite:&w alpha:&a];
//        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
//        self.gyro.accValueY.textColor = [self.gyro.accValueY.textColor colorWithAlphaComponent:a];
//        
//        [self.gyro.accValueZ.textColor getWhite:&w alpha:&a];
//        if (a > MIN_ALPHA_FADE) a -= ALPHA_FADE_STEP;
//        self.gyro.accValueZ.textColor = [self.gyro.accValueZ.textColor colorWithAlphaComponent:a];
//    }
}


-(void) logValues:(NSTimer *)timer {
    NSString *date = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                    dateStyle:NSDateFormatterShortStyle
                                                    timeStyle:NSDateFormatterMediumStyle];
    self.currentVal.timeStamp = date;
    sensorTagValues *newVal = [[sensorTagValues alloc]init];
    newVal.tAmb = self.currentVal.tAmb;
    newVal.tIR = self.currentVal.tIR;
    newVal.accX = self.currentVal.accX;
    newVal.accY = self.currentVal.accY;
    newVal.accZ = self.currentVal.accZ;
    newVal.gyroX = self.currentVal.gyroX;
    newVal.gyroY = self.currentVal.gyroY;
    newVal.gyroZ = self.currentVal.gyroZ;
    newVal.magX = self.currentVal.magX;
    newVal.magY = self.currentVal.magY;
    newVal.magZ = self.currentVal.magZ;
    newVal.press = self.currentVal.press;
    newVal.humidity = self.currentVal.humidity;
    newVal.timeStamp = date;
    
    [self.vals addObject:newVal];
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    NSLog(@"Finished with result : %u error : %@",result,error);
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)sendMail:(id)sender {
    NSLog(@"Mail button pressed");
    NSMutableString *sensorData = [[NSMutableString alloc] init];
    [sensorData appendString:@"Timestamp,Ambient Temperature,IR Temperature,Accelerometer X-Axis,Accelerometer Y-Axis,Accelerometer Z-Axis,Barometric Pressure,Relative Humidity,Gyroscope X,Gyroscope Y,Gyroscope Z,Magnetometer X, Magnetometer Y, Magnetometer Z\n"];
    for (int ii=0; ii < self.vals.count; ii++) {
        sensorTagValues *s = [self.vals objectAtIndex:ii];
        [sensorData appendFormat:@"%@,%0.1f,%0.1f,%0.2f,%0.2f,%0.2f,%0.0f,%0.1f,%0.1f,%0.1f,%0.1f,%0.1f,%0.1f,%0.1f\n",s.timeStamp,s.tAmb,s.tIR,s.accX,s.accY,s.accZ,s.press,s.humidity,s.gyroX,s.gyroY,s.gyroZ,s.magX,s.magY,s.magZ];
    }
    
    MFMailComposeViewController *mFMCVC = [[MFMailComposeViewController alloc]init];
    if (mFMCVC) {
        if ([MFMailComposeViewController canSendMail]) {
            mFMCVC.mailComposeDelegate = self;
            [mFMCVC setSubject:@"Data from BLE Sensor"];
            [mFMCVC setMessageBody:@"Data from sensor" isHTML:NO];
            [self presentViewController:mFMCVC animated:YES completion:nil];
            
            [mFMCVC addAttachmentData:[sensorData dataUsingEncoding:NSUTF8StringEncoding] mimeType:@"text/csv" fileName:@"Log.csv"];
        }
        else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Mail error" message:@"Device has not been set up to send mail" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
    }
}

@end
